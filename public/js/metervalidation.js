let meterData = []; //the data that is pushed into this array is used for the getModel function
let reviewedModels = []; //the data that is pushed to this array is used to determine if a meter has been reviewed after the reviewed button has been pressed
let replaceData = []; //the data that is pushed to this array is used to send the replace data back to s3 using the user input from the replace data table that is below the charts.
let updateModelStart; //These two variables are used for the google slider chart. These two are global variables so that copyModelDate function has access
let updateModelEnd; //to these variables outside of the getModel function

$(document).ready(() => {
  //Initialize Datepicker for model start and end and analysis start and end
  const dateInput_1 = $(".datepicker");

  dateInput_1.datepicker({
    changeYear: true,
    dateFormat: "yy-mm-dd",
  });

  //initializes the overlay and loading spinner when searching for a model. This class is attached to the RUN MODEL button as well as the SUBMIT button on the replace data table
  $(".load").on("click", function() {
    $("#overlay").fadeIn();
  });

  //This is supposed to delete the records in the table reviewed_model at the end of the month. I didnt take into account that someone needs to visit the
  //metervalidation app in order for this script to run. This would be best implemented using a trigger function in pgAdmin to delete records at the end of the month.

  // const lastDay = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1);

  // if (d === lastDay) {
  //   $.ajax({
  //     url: "/deleteModels",
  //     type: "DELETE",
  //   });
  // }
 
  //this function is responsible for using the ajax call on page load to populate the meter selection table.
  //meter selection table is located under the view folder under meterValidation folder in cleaning.handlebars file. It has the class of meterData
  //it also pulls data from the reviewed_models table in postgres to indicate if a meter was previously reviewed.
  const getToClean = () => {
    const start = new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 1,
      1
    )
      .toISOString()
      .slice(0, 10)
  
      const end = new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        0
      )
        .toISOString()
        .slice(0, 10)
    //these two ajax calls are combined in a .when() statement. This means that before data is returned for either api, both ajax calls have to finish calling their separate api's.
    $.when(
      //this ajax call is to populate the meter selection table with the list of meters
      $.ajax({
        //this route can be found under the routes folder under meterValidation folder, in apiGateway.js
        //The controller for this route is set up so that if $("#filter-steward").val() is empty then it returns all data from the api.
        //the $("#filter-steward").val() will always be empty when the page first loads or is refreshed so it will always return every meter back from the api on the first visit.
        //If $("#filter-steward").val() contains a stewards name then only the meters will be returned that are tied to that steward.
        url: "/getToClean",
        type: "GET",
        data: {
          //start timestamp is first day of previous month
          startTimestamp: start,
          //end timestamp is last day of previous month
          endTimestamp: end,
          steward: $("#filter-steward").val(),
        },
      }),
      //this ajax call pulls the data from the reviewed_models table in postgres to check for any models that have been reviewed.
      $.ajax({
        //this route can be found under the routes folder under meterValidation folder, in reviewedModels.js
        url: "/getReview",
        type: "GET",
        data: {
          //start timestamp is first day of previous month
          startTimestamp: start,
          //end timestamp is last day of previous month
          endTimestamp: end,
        }
      }),
      //toClean holds data for the /getToClean route and review holds data for the /getReview route
    ).then((toClean, reviewed) => {
      if (
        toClean[0] === "Request failed with status code 504" ||
        toClean[0] === "Request failed with status code 500"
      ) {
        getToClean();
        //this else statement extends all the way to the $('.remove0) function
      } else {
        console.log(start, end)
        console.log(toClean);
        $(".meterSelectionSpinner").hide();
        $(".apply").html("Apply");
        const meter = toClean[0].body.meter;
        const flagCount = toClean[0].body.flag_count;
        const building = toClean[0].body.building_abbreviation;
        const building_number = toClean[0].body.building_number;
        const commodity = toClean[0].body.commodity_tag;
        const notes = toClean[0].body.notes;
        //This maps through the meter variable which is set to the getAlarm[0].body.meter. It will then populate the empty tablebody for the meter selection list.
        meter.map((meter, index) => {
          $(".meterList").append(`
            <tr>
              <td style="width: 10px;" class='position text-center'><input class="form-check-input" type="radio" name="meterSelect" id="radioNoLabel1" aria-label="Select a meter"></td>
              <td>${building[index]}</td>
              <td>${meter}</td>
              <td>${flagCount[index]}</td>
              <td id=${meter}>No</td>
              <td style="display: none">${notes[index]}</td>
              <td style="display: none">${building_number[index]}</td>
              <td style="display: none">${commodity[index]}</td>
            </tr>
            `);
        });

        //This click event displays the notes underneath the confirm meter selection button that are associated with the meter when a radio button is clicked.
        $('input[name="meterSelect"]').on("click", function() {
          const notes = $(this)
            .closest("tr")
            .children("td:eq(5)")
            .text();
          const meter = $(this)
            .closest("tr")
            .children("td:eq(2)")
            .text();
          const buildingNumber = $(this)
            .closest("tr")
            .children("td:eq(6)")
            .text();
          $("#modelNotes").show();
          $(".notesLabel").html(meter);
          $(".notesLabelBuildingNumber").html(buildingNumber);
          if (notes === "null") {
            $(".modelNotes").val("No Notes");
          } else {
            $(".modelNotes").val(notes);
          }
        });

        //maps through the review data in the combined ajax calls.
        //each cell in the third index position in the table above has a unique id containing the meter name.
        //if the meter in reviewedModels matches any of the id's in the table cell, it applies the modifications below.
        const reviewed_meters = reviewed[0].body.meter
        reviewed_meters.map((reviewed_meter, index) => {
          $(`#${reviewed_meter}`).text("Yes");
          $(`#${reviewed_meter}`)
            .parent("tr")
            .css("background-color", "#00b74a");
          reviewed.push(reviewed_meter);
        });

        //This function automatically sorts the meter selection table by building and meter ASC every time the table loads.
        const t = (tr, i) => tr.cells[i].textContent;
        $(".meterList tr")
          .get()
          .sort(
            (a, b) =>
              t(a, 1).localeCompare(t(b, 1)) || t(a, 2).localeCompare(t(b, 2))
          )
          .map((tr) => $(".meterList").append(tr));

        //initialize the search box above the meter selection table.
        //the value of the search box is transformed to uppercase so the user doesnt have to do it themselves.
        $("#search").on("keyup", function() {
          const value = $(this)
            .val()
            .toUpperCase();
          $(".meterList tr").each(function() {
            $row = $(this);

            $row.find("td").each(function() {
              const id = $(this).text();
              if (id.indexOf(value) !== 0) {
                $row.hide();
              } else {
                $row.show();
                return false;
              }
            });
          });
        });
        //This on change event filters for meters that have or havent been reviewed using the reviewed select input above the meter selection table.
        //If the value of the select input is "No" then all meters that havent been reviewed will be displayed and all the meters that have been reviewed will be hidden.
        //If the value of the select input is "Yes" then all meters that have been reviewed will be displayed and all the meters that havent been reviewed will be hidden.
        //If the value of the select input is "All"  then all meters will be displayed
        $(".reviewedFilter").on("change", function() {
          const value = $(this).val();
          $(".meterData tbody tr").each(function() {
            if (value === "No") {
              $(this)
                .find("td:eq(4):contains(No)")
                .closest("tr")
                .show();
              $(this)
                .find("td:eq(4):contains(Yes)")
                .closest("tr")
                .hide();
            } else if (value === "Yes") {
              $(this)
                .find("td:eq(4):contains(No)")
                .closest("tr")
                .hide();
              $(this)
                .find("td:eq(4):contains(Yes)")
                .closest("tr")
                .show();
            } else {
              $(this)
                .find("td:eq(4):contains(No)")
                .closest("tr")
                .show();
              $(this)
                .find("td:eq(4):contains(Yes)")
                .closest("tr")
                .show();
            }
          });
        });
        //this on change function hides rows where # flagged days is equal to 0 if the value of the select input is "Yes"
        $(".remove0").on("change", function() {
          const value = $(this).val();
          $(".meterData tbody tr").each(function() {
            if (value === "Yes") {
              $(this)
                .find("td:eq(3):contains(0)")
                .closest("tr")
                .hide();
            } else {
              $(this)
                .find("td:eq(3):contains(0)")
                .closest("tr")
                .show();
            }
          });
        });
      }
    });
  };
  //getToClean is called on page load
  getToClean();

  //Calls the getToClean function only if the user wants to filter by a specific steward.
  //This activates after the user has selected a steward to filter by and then has press the apply button that is appended to the steward select input
  //Also everytime the apply button is clicked the meter selection table is refreshed using the load() function to refresh the meter selection table to display the stewards meters
  //If there are any changes to the steward personnel, then their names will need to be removed/added from/to the select input that filters the stewards
  $(".filterBySteward").on("click", function() {
    $(".meterSelectionSpinner").show();
    $(".meterData").load(location.href + " .meterData");
    getToClean();
  });

  //this function is used to review models. When the reviewed button is clicked the building and meter are sent to the meter_tag_olsr_model_review table in postgres.
  //then the reviewed model button is disabled and a checkmark is appended to the button to indicate that the model has been reviewed.
  const postReview = (body) => {
    $.ajax({
      url: "/postReview",
      type: "POST",
      data: {
        building_number: body.building.building_number,
        meter: body.meter,
        commodity_tag: body.commodity.tag,
        train_start: body.model.train_start,
        train_end: body.model.train_end,
        analysis_start: body.model.analysis_start,
        analysis_end: body.model.analysis_end,
        x: body.model.x,
        auto_ignored_percentage: body.model.missing_value.auto_ignored_percentage,
        base_temperature: body.model.base_temperature,
        r2: body.model.max_train_r2,
        slope: body.model.slope,
        intercept: body.model.intercept,
        std: body.model.std.train,
      },
    }).then(() => {
      $(".reviewed").html(`Reviewed <i class="far fa-check-circle"></i>`);
      $(".reviewed").attr("disabled", true);
      $(".reviewed").addClass("btn-success");
      //Each table cell in the Reviewed column has an id of whatever the meter is in that row. When the reviewed button is clicked on the model, it takes the
      //value of the currentMeter class and uses that as an id selector for the table cell for the Reviewed column in the meter selection table.
      //It will then change the html output from 'No' to 'Yes' and then change the background color of the entire row to green. This is the indicate to the user that they
      //have successfully reviewed that model
      $(`#${$(".currentMeter").text()}`).text("Yes");
      $(`#${$(".currentMeter").text()}`)
        .closest("tr")
        .css("background-color", "#00b74a");
    });
  };

  //This on change function will always set the model end date to 364 days ahead of the model start date
  $(".modelStart").on("change", function() {
    const modelStartDate = $(this).datepicker("getDate");
    modelStartDate.setDate(modelStartDate.getDate() + 364);
    $(".modelEnd").datepicker("setDate", modelStartDate);
  });

  //This on change function will empty the global array variable meterData whenever a new radio button is selected in the meter selection table
  //This is necessary so that only the meter you select is used to search for the model, and not every meter you may click.
  $(".meterData").on("change", $('input[name="meterSelect"]'), function() {
    meterData = [];
  });

  //This is the on click event for the confirm meter selection button
  $(".confirmMeter").click(function() {
    //when the confirm meter selection button is clicked, the model dates will automatically be determined.
    //model start will be defaulted to the first day of the previous month of the previous year
    $(".modelStart").datepicker(
      "setDate",
      new Date(new Date().getFullYear() - 1, new Date().getMonth() - 1, 1)
    );
    //model end will be defaulted to the default model start date + 364 days
    $(".modelEnd").datepicker(
      "setDate",
      new Date(new Date().getFullYear() - 1, new Date().getMonth() - 1, +364)
    );
    //analysis start date will be defaulted to the first day of the previous month of the current year
    $(".analysisStart").datepicker(
      "setDate",
      new Date(new Date().getFullYear(), new Date().getMonth() - 1, 1)
    );
    //analysis end date will be defaulted to the last day of the previous month of the current year
    $(".analysisEnd").datepicker(
      "setDate",
      new Date(new Date().getFullYear(), new Date().getMonth(), 0)
    );

    //When the confirm meter selection button is clicked, the data for that specific row will be pushed into the global array variable meterData
    $('input[name="meterSelect"]:checked', $(".meterData")).each(function() {
      meterData.push({
        meter: $(this)
          .closest("tr")
          .children("td:eq(2)")
          .text(),
        building_number: $(this)
          .closest("tr")
          .children("td:eq(6)")
          .text(),
        commodity_tag: $(this)
          .closest("tr")
          .children("td:eq(7)")
          .text(),
        note: $(this)
          .closest("tr")
          .children("td:eq(5)")
          .text(),
      });
    });
    //This populates the Current Meter Selection element with the confirmed meter
    $(".meterSelection").text(meterData[0].meter);
  });

  //This on click event is bound to the RUN MODEL button. If no meter has been selected in the meter selection table and the global array variable meterData is empty
  //then an alert will popup indicating to the user that they havent selected a meter
  $(".loadModel").on("click", function(e) {
    e.preventDefault();
    if (meterData.length === 0) {
      alert(
        'No meter selected. Please click "Confirm Meter Selection" to confirm your selected meter'
      );
      $("#overlay").hide();
    } else {
      //everytime the RUN MODEL button is clicked, the divs that contain the chart data, the google chart data that contains consumption data, and the replace data table
      //will be refreshed so that the new data will be displayed when a new model is run

      //This is the two consumption charts.
      $("#chartData").load(location.href + " #chartData");
      //this is for the replace data table beneath the charts
      $(".tableData").load(location.href + " .tableData");
      //This is for the google sliding chart
      $("#consumptionRow").load(location.href + " #consumptionRow");
      //calls the getModel function on click
      getModelData();
    }
  });

  //This is the main function that displays the data for the historic chart, the two consumption charts, and the replacement table data.
  const getModelData = function() {
    // These two date variables are responsible for supplying dates to the historic google sliding chart.
    const endTime = new Date().toISOString().slice(0, 10);
    const startTime = new Date(
      new Date().getFullYear() - 3,
      new Date().getMonth() - 1,
      1
    )
      .toISOString()
      .slice(0, 10);
    const modelStart = $(".modelStart").val();
    const modelEnd = $(".modelEnd").val();
    const analysisStart = $(".analysisStart").val();
    const analysisEnd = $(".analysisEnd").val();
    const buildingNumber = meterData[0].building_number;
    const commodity = meterData[0].commodity_tag;
    const meter = meterData[0].meter;
    //There are three ajax calls connected using a .when() statement. This means that data will only be returned once all three ajax calls have been successfully finished.
    $.when(
      //This ajax call is responsible for populating the two consumption charts and the replace data table below those two charts. This route can be found under the routes folder, under meterValidation folder, in apiGateway.js
      $.ajax({
        url: "/getValidation",
        method: "GET",
        data: {
          buildingNumber: buildingNumber,
          commodity: commodity,
          meter: meter,
          trainStart: modelStart,
          trainEnd: modelEnd,
          analysisStart: analysisStart,
          analysisEnd: analysisEnd,
        },
      }),
      //This ajax call is responsible for populating the historic google sliding chart. This route can be found under the routes folder, under meterValidation folder, in apiGateway.js
      $.ajax({
        url: "/getTimeSeries",
        method: "GET",
        data: {
          buildingNumber: buildingNumber,
          commodity: commodity,
          meter: meter,
          startTimestamp: startTime,
          endTimestamp: endTime,
        },
      }),
      //This route is responsible for returning the saved attributes from the table meter_olsr_model in postgres. This route can be found under the routes folder, under meterValidation folder, in meterAttribute.js
      $.ajax({
        type: "GET",
        url: "/getModel",
        data: {
          buildingNumber: buildingNumber,
          meter: meter,
        },
      })
    ).then((validation, timeSeries, existingModel) => {
      console.log(validation);
      //This if statement says if any of the ajax calls timeout with a 504 error, then rerun all ajax calls until they all responsd with data.
      if (
        validation[0] === "Request failed with status code 504" ||
        timeSeries[0] === "Request failed with status code 504" ||
        existingModel[0] === "Request failed with status code 504"
      ) {
        getModelData();
        $(".overlayMessage").text(
          "Server not responding, trying your search again. Please do not refresh the page"
        );
        //This else if statement is neccessary because if a certain model doesnt have any data to response, the getModel response will look like the string below.
        //if the getModel response returns with this empty body, then an alert box pops up and says no data has been returned for this model.
        //However it is very rare for a model to return empty data. If it does, consult with Kingsley to see if there is an error in the API.
      } else if (
        validation[0] ===
        '{\n    "statusCode": ,\n    "body": ,\n    "headers": {\n            }\n}'
      ) {
        $("#overlay").fadeOut();
        alert("No data has been returned for this model");
      } else {
        //saveAttributes is connected to the save attributes button at the top of the page. After the user saves a models attributes, this button will become disabled.
        // This selector ensures that when the user is done with their current model and searches for another one, the save attribute button becomes re-enabled.
        $(".saveAttributes").attr("disabled", false);
        //displaydata is the container that holds all of the information that is returned with the getModel function.
        $(".displayData").show();
        //modelNotes are notes that are attached to the model that are returned with the getToClean function.
        //After the model has returned data, the user will be able to edit and submit a new model note.
        $(".modelNotes").attr("disabled", false);
        $(".editModelNote").show();
        //This if else statement is responsible for populating the saved attributes row at the top of the page.
        //if the response for getModel is empty, then the savedAttributes row is hidden. If the response is not empty, then the savedAttributes row is populated with the data that is returned.
        if (existingModel[0].length === 0) {
          $(".savedAttributes").hide();
        } else {
          $(".savedAttributes").show();
          $(".savedBaseTemp").html(
            existingModel[0].body.base_temperature === null
              ? "--"
              : existingModel[0].body.base_temperature
          );
          $(".savedAutoIgnored").html(
            existingModel[0].body.auto_ignored_percentage + "%"
          );
          $(".savedSlope").html(existingModel[0].body.slope);
          $(".savedIntercept").html(existingModel[0].body.intercept);
          $(".savedR2").html(existingModel[0].body.r2);
          $(".savedStdDev").html(existingModel[0].body.std);
          $(".savedStart").html(existingModel[0].body.train_start);
          $(".savedEnd").html(existingModel[0].body.train_end);
        }
        //The review button is dynamically created every time a new model is ran.
        $(".reviewedButton")
          .html(` <button style='margin-bottom: 20px;' type="button"
        class="btn btn-primary reviewed text-center">Reviewed</button>`);
        //When the reviewed button is clicked, it will push the current models meter into the global array variable reviewedModels.
        $(".reviewed").on("click", function() {
          reviewedModels.push(validation[0].body.meter);
          postReview(validation[0].body);
        });
        //If the user wants to revisit a model that has been marked reviewed during the same session, then this if statement checks the global array variable for any meters that are stored in it
        //If the models meter matches the meter that is stored in the global array, then the following properties are added to the review button
        if (reviewedModels.includes(validation[0].body.meter)) {
          $(".reviewed").attr("disabled", true);
          $(".reviewed").addClass("btn-success");
          $(".reviewed").html('Reviewed <i class="far fa-check-circle"></i>');
        }

        //in the response for getValidation, it will return data that dates all the way back to the model start date. Since the only date range of information we want to display to the user
        //is the analysis period, I use this variable to get the index of the analysis start date and then use that to slice the rest of the data arrays.
        const analysisIndex = validation[0].body.model.data.timestamp.indexOf(
          $(".analysisStart").val()
        );
        const lowLimit = validation[0].body.model.data.predicted_value_lower_bound.slice(
          analysisIndex
        );
        const xTemp = validation[0].body.model.data.average_dry_bulb_temperature.slice(
          analysisIndex
        );
        const highLimit = validation[0].body.model.data.predicted_value_upper_bound.slice(
          analysisIndex
        );
        const rawValue = validation[0].body.model.data.raw_value.slice(
          analysisIndex
        );
        const xTimestamp = validation[0].body.model.data.timestamp.slice(
          analysisIndex
        );
        //This will reset the overlay message if it gets changed from a different function.
        $(".overlayMessage").text("Getting data, this will take a few seconds");
        //This hides the overlay after the API's have been successfully called and data is populated.
        $("#overlay").fadeOut();
        //.baseTemp down to .currentCommodity populates the current model attributes headers at the top of the page, as well as building name, meter variable, and the meter name
        $(".baseTemp").html(
          validation[0].body.model.base_temperature === null
            ? "--"
            : validation[0].body.model.base_temperature
        );
        $(".autoIgnored").html(
          parseFloat(
            validation[0].body.model.missing_value.auto_ignored_percentage
          ).toFixed(0) + "%"
        );
        $(".slope").html(parseFloat(validation[0].body.model.slope).toFixed(2));
        $(".intercept").html(
          parseFloat(validation[0].body.model.intercept).toFixed(2)
        );
        $(".r2").html(
          parseFloat(validation[0].body.model.max_train_r2).toFixed(2)
        );
        $(".stdDev").html(
          parseFloat(validation[0].body.model.std.train).toFixed(2)
        );
        $(".start").html(modelStart);
        $(".end").html(modelEnd);
        $(".meterVariable").html(validation[0].body.model.x.toUpperCase());
        $(".currentBuildingName").text(
          validation[0].body.building.building_abbreviation
        );
        $(".currentMeter").text(validation[0].body.meter);
        $(".currentCommodity").text(validation[0].body.commodity.tag);
        //displays the save attributes button which is set to be hidden by default
        $(".saveAttributes").show();
        //This is how the data is arranged to display on the two charts.
        //x is the x variable on the chart and y is the y variable on the chart
        //type determines what the variable type will be on the x-axis. In our case we use Number and String.
        const chartData = (x, y, type) => {
          let result = {};
          //takes the x variable and y variable and joins them together into an object using the result variable.
          //end result would look like this [{x:y}]
          x.forEach((key, i) => (result[key] = y[i]));
          //This variable then takes the data stored in the result object variable and turns them into arrays
          //So instead of the data looking like {x:y}, it now looks like [x, y]. This is necessary because the datapoints must be sorted in order before being placed onto the charts.
          //data obj do not support and sorting functions, or at least none that I could find, so the data objects must be reformatted into an array.
          const dataPoints = Object.keys(result).map(function(key) {
            return [type(key), result[key]];
          });
          //This will then sort the new dataPoints array ASC using the x variable
          dataPoints.sort(function(a, b) {
            return a[0] - b[0];
          });

          //This will map through the newly sorted dataPoints array and plot the x and y variables onto the charts using this object format.
          //index 0 is the x variable and index y is the y variable
          return dataPoints.map((a) => {
            return { x: a[0], y: a[1] };
          });
        };
        //If the commodity of the selected meter is water, then the consumption chart that shows meter vs temp is hidden, and the consumption chart that shows meter vs date is expanded to take the entire space.
        if (validation[0].body.model.x === "occ") {
          $("#hideIfWater").hide();
          $("#fullWidth").attr("class", "col-xxl-12");
        }
        //Config settings for the fist consumption chart. Meter vs Temp
        const config = {
          data: {
            datasets: [
              {
                type: "scatter",
                label: "Meter VS. Temp",
                //Uses the chartData from above. xTemp variable is the x value, rawValue variable is the y value, and the data type of the x axis is a Number
                data: chartData(xTemp, rawValue, Number),
                backgroundColor: "#00FFFF",
                pointRadius: 5,
              },
              {
                type: "line",
                label: "High Limit",
                //Uses the chartData from above. xTemp variable is the x value, highLimit variable is the y value, and the data type of the x axis is a Number
                data: chartData(xTemp, highLimit, Number),
                fill: false,
                pointRadius: 0,
                tension: 0.1,
                borderColor: "#d9534f",
              },
              {
                type: "line",
                label: "Low Limit",
                //Uses the chartData from above. xTemp variable is the x value, lowLimit variable is the y value, and the data type of the x axis is a Number
                data: chartData(xTemp, lowLimit, Number),
                fill: false,
                pointRadius: 0,
                tension: 0.1,
                borderColor: "#ffcc66",
              },
            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
              legend: {
                labels: {
                  color: "white",
                },
              },
            },
            scales: {
              y: {
                beginAtZero: true,
                ticks: {
                  color: "white",
                },
                grid: {
                  color: "black",
                },
              },
              x: {
                ticks: {
                  color: "white",
                },
                grid: {
                  color: "black",
                },
              },
            },
          },
        };
        //initializes the first chart meter vs temp
        const ctx = document.getElementById("myChart").getContext("2d");
        const myChart = new Chart(ctx, config);

        //Config settings for the second consumption chart. Meter vs Dates
        const config2 = {
          data: {
            datasets: [
              {
                type: "scatter",
                label: "Meter VS. Dates",
                //Uses the chartData from above. xTemp variable is the x value, rawValue variable is the y value, and the data type of the x axis is a String
                //X value is a string because the xTimestamp comes back as a string from the API
                data: chartData(xTimestamp, rawValue, String),
                pointRadius: 5,
                backgroundColor: "#00FFFF",
              },
              {
                type: "line",
                label: "High Limit",
                //Uses the chartData from above. xTemp variable is the x value, highLimit variable is the y value, and the data type of the x axis is a String
                data: chartData(xTimestamp, highLimit, String),
                fill: false,
                pointRadius: 0,
                borderColor: "#d9534f",
                tension: 0.1,
              },
              {
                type: "line",
                label: "Low Limit",
                //Uses the chartData from above. xTemp variable is the x value, lowLimit variable is the y value, and the data type of the x axis is a String
                data: chartData(xTimestamp, lowLimit, String),
                fill: false,
                pointRadius: 0,
                borderColor: "#ffcc66",
                tension: 0.1,
              },
            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
              legend: {
                labels: {
                  color: "white",
                },
              },
            },
            scales: {
              x: {
                type: "time",
                time: {
                  unit: "day",
                  tooltipFormat: "MMM dd, yyyy",
                },
                ticks: {
                  color: "white",
                },
                grid: {
                  color: "black",
                },
              },
              y: {
                beginAtZero: true,
                ticks: {
                  color: "white",
                },
                grid: {
                  color: "black",
                },
              },
            },
          },
        };
        //initializes the second chart meter vs dates
        const ctx2 = document.getElementById("myChart2").getContext("2d");
        const myChart2 = new Chart(ctx2, config2);
        
        //google slider historic chart
        //The code for this chart extends down to line 838
        const consumptionResult = {};
        const consumptionTimestamp = timeSeries[0].body.timestamp;
        const consumptionValue = timeSeries[0].body.value;
        consumptionTimestamp.forEach(
          (key, i) => (consumptionResult[key] = consumptionValue[i])
        );

        const newConsumptionArr = Object.keys(consumptionResult).map(function(
          key
        ) {
          return [String(key), consumptionResult[key]];
        });

        google.load("visualization", "1", {
          packages: ["controls", "charteditor"],
        });
        google.setOnLoadCallback(drawChart);

        function drawChart() {
          const data = new google.visualization.DataTable();
          data.addColumn("date");
          data.addColumn("number");

          newConsumptionArr.forEach((item) => {
            data.addRow([new Date(item[0]), item[1]]);
          });

          const dash = new google.visualization.Dashboard(
            document.getElementById("dashboard")
          );

          const control = new google.visualization.ControlWrapper({
            controlType: "ChartRangeFilter",
            containerId: "control_div",
            state: {
              range: {
                start: new Date($(".modelStart").val()),
                end: new Date($(".modelEnd").val()),
              },
            },
            options: {
              filterColumnIndex: 0,
              ui: {
                chartType: "ScatterChart",
                chartOptions: {
                  height: "100",
                  width: "90%",
                  colors: ["#15A0C8"],
                  backgroundColor: {
                    fill: "#48555F",
                  },
                  chartArea: {
                    height: "100",
                    width: "90%",
                  },
                  hAxis: {
                    baselineColor: "#FFFFFF",
                    gridlineColor: "#FFFFFF",
                    textStyle: { color: "#FFF" },
                  },
                },
              },
            },
          });
          const chartOptions = {
            legend: "none",
            tooltip: {
              isHtml: true,
              trigger: "hover",
            },
            pointSize: 10,
            dataOpacity: 1,
            colors: ["#15A0C8"],
            vAxis: {
              baselineColor: "#000000",
              gridlineColor: "#000000",
              textStyle: { color: "#FFF" },
            },
            hAxis: {
              baselineColor: "#000000",
              gridlineColor: "#000000",
              textStyle: { color: "#FFF" },
            },
            height: 600,
            width: 1000,
            backgroundColor: {
              fill: "#48555F",
            },
          };

          const chart = new google.visualization.ChartWrapper({
            chartType: "ScatterChart",
            containerId: "chart_div",
            options: chartOptions,
          });

          function setOptions(wrapper) {
            wrapper.setOption("width", "90%");
            wrapper.setOption("chartArea.width", "90%");
          }

          setOptions(chart);

          dash.bind([control], [chart]);
          //This button is responsible for showing and hiding the google chart.
          $(".show-hide").on("click", function() {
            const icon = this.querySelector("i");
            const text = this.querySelector("span");
            dash.draw(data);
            if (icon.classList.contains("fa-eye")) {
              icon.classList.remove("fa-eye");
              icon.classList.add("fa-eye-slash");
              text.innerHTML = "Hide Historic";
            } else {
              icon.classList.remove("fa-eye-slash");
              icon.classList.add("fa-eye");
              text.innerHTML = "Show Historic";
            }
          });

          google.visualization.events.addListener(
            control,
            "statechange",
            function() {
              const v = control.getState();
              document.getElementById("dbgchart").innerHTML =
                v.range.start.toISOString().slice(0, 10) +
                " to " +
                v.range.end.toISOString().slice(0, 10);
              updateModelStart = v.range.start.toISOString().slice(0, 10);
              updateModelEnd = v.range.end.toISOString().slice(0, 10);
              return 0;
            }
          );
        }

        //This copies the selected dates on the historic chart to the model start and end dates so the user can run a model with those dates
        $(".copyModelDates").on("click", () => {
          $(".modelStart").val(updateModelStart);
          $(".modelEnd").val(updateModelEnd);
        });

        //This function is responisble for populating the replace data table underneath the charts.
        $(function() {
          const dates = validation[0].body.model.data.timestamp.slice(
            analysisIndex
          );
          const temperature = validation[0].body.model.data.average_dry_bulb_temperature.slice(
            analysisIndex
          );
          const x = validation[0].body.model.data.degree_day.slice(analysisIndex);
          const occ = validation[0].body.model.data.is_occupied.slice(
            analysisIndex
          );
          const meter = validation[0].body.model.data.raw_value.slice(
            analysisIndex
          );
          const expected = validation[0].body.model.data.predicted_value.slice(
            analysisIndex
          );
          const replacement = validation[0].body.model.data.replacement_value.slice(
            analysisIndex
          );
          const reason = validation[0].body.model.data.replacement_reason.slice(
            analysisIndex
          );
          const notes = validation[0].body.model.data.replacement_notes.slice(
            analysisIndex
          );
          const lowerBound = validation[0].body.model.data.predicted_value_lower_bound.slice(
            analysisIndex
          );
          const upperBound = validation[0].body.model.data.predicted_value_upper_bound.slice(
            analysisIndex
          )

          //maps through the data variables and creates a table based on the amount of data
          dates.map((date, index) => {
            $(".tableBody").append(`
              <tr>
                <td class='position'><input class="form-check-input edit" name='edit' type="checkbox"></td>
                <td class='date'>${date}</td>
                <td>${temperature[index]}</td>
                <td>${
                  $(".meterVariable").text() === "OCC"
                    ? occ[index]
                    : parseFloat(x[index]).toFixed(0)
                }</td>
                <td id=${Math.trunc(
                  meter[index]
                )} class='meterReading'>${Math.trunc(meter[index])}</td>
                <td class='expected'>${parseFloat(expected[index]).toFixed(
                  0
                )}</td>
                <td >${
                  replacement[index] === null
                    ? "-"
                    : parseFloat(replacement[index]).toFixed(0)
                }</td>
                <td>${reason[index] === null ? "-" : reason[index]}</td>
                <td>${notes[index] === null ? "-" : notes[index]}</td>
                <td class="upperBound" style='display: none'>${parseFloat(
                  upperBound[index]
                ).toFixed(0)}</td>
                <td class="lowerBound" style='display: none'>${parseFloat(
                  lowerBound[index]
                ).toFixed(0)}</td>
              </tr>`);
          });
          $(".x").html(validation[0].body.model.x.toUpperCase());
          $(".tableData tbody tr").each(function() {
            const meterReading = $(this)
              .find(".meterReading")
              .html();
            const lowerBound = $(this)
              .find(".lowerBound")
              .html();
            const upperBound = $(this)
              .find(".upperBound")
              .html();
            if (
              parseInt(meterReading, 10) < parseInt(lowerBound, 10) ||
              parseInt(meterReading, 10) === parseInt(lowerBound, 10)
            ) {
              $(this).css("background-color", "#F0AD4E");
              if ($(this).index() === 0) {
                $(this)
                  .children("td:eq(0)")
                  .append(
                    `<a  href="#" class="warning firstRow" data-tool-tip="Low Limit: ${lowerBound}"><i class="fas fa-exclamation-circle fa-2x"></i></a>`
                  );
              } else {
                $(this)
                  .children("td:eq(0)")
                  .append(
                    `<a  href="#" class="warning" data-tool-tip="Low Limit: ${lowerBound}"><i class="fas fa-exclamation-circle fa-2x"></i></a>`
                  );
              }
            }

            if (
              parseInt(meterReading, 10) > parseInt(upperBound, 10) ||
              parseInt(meterReading, 10) === parseInt(upperBound, 10)
            ) {
              $(this).css("background-color", "#d9534f");
              if ($(this).index() === 0) {
                $(this)
                  .children("td:eq(0)")
                  .append(
                    `<a  href="#" class="warning firstRow" data-tool-tip="High Limit: ${upperBound}"><i class="fas fa-exclamation-circle fa-2x"></i></a>`
                  );
              } else {
                $(this)
                  .children("td:eq(0)")
                  .append(
                    `<a  href="#" class="warning" data-tool-tip="High Limit: ${upperBound}"><i class="fas fa-exclamation-circle fa-2x"></i></a>`
                  );
              }
            }

            if (
              $(this)
                .children("td:eq(4)")
                .text() ===
              $(this)
                .next()
                .children("td:eq(4)")
                .text()
            ) {
              $(this)
                .next()
                .css("background-color", "#0275d8");
            }
          });
          const checkboxes = document.querySelectorAll(
            '.tableData input[type="checkbox"]'
          );
          let lastChecked;

          function handleCheck(e) {
            let inBetween = false;
            if (e.shiftKey && this.checked) {
              checkboxes.forEach((checkbox) => {
                if (checkbox === this || checkbox === lastChecked) {
                  inBetween = !inBetween;
                }
                if (inBetween) {
                  checkbox.checked = true;
                }
              });
            }
            lastChecked = this;
          }

          checkboxes.forEach((checkbox) =>
            checkbox.addEventListener("click", handleCheck)
          );
        });
      }
    });
  };

  const submitOngoingIssue = () => {
    $.ajax({
      url: "/postOngoingIssue",
      type: "POST",
      data: {
        building_number: 
          $(".notesLabelBuildingNumber").html(),
        meter: $(".notesLabel").html(),
        notes: $(".modelNotes").val(),
      },
    }).then(() => {
      $(`#${$(".notesLabel").html()}`)
        .siblings("td:eq(5)")
        .text($(".modelNotes").val());
      $(".editModelNote").html(
        `<i style="margin: 0 auto" class="far fa-check-circle fa-2x text-center"></i>`
      );
      setTimeout(() => {
        $(".editModelNote").html("Submit Note");
      }, 4000);
    });
  };

  $(".editModelNote").on("click", () => {
    $(this)
      .html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    <span class="visually-hidden">Loading...</span>`);
    submitOngoingIssue();
  });

  $(".copyAttributeDates").on("click", () => {
    $(".modelStart").val($(".savedStart").html());
    $(".modelEnd").val($(".savedEnd").html());
  });

  $("#reason").on("change", function() {
    $("#submitReplacement").removeAttr("disabled");
  });

  $("#submitReplacement").click(function() {
    $("input:checkbox:checked", $(".tableData"))
      .each(function() {
        replaceData.push({
          Date: $(this)
            .closest("tr")
            .find(".date")
            .text(),
          Expected: $(this)
            .closest("tr")
            .find(".expected")
            .text(),
          index: $(this)
            .closest("tr")
            .index(),
        });
      })
      .get();

    const checked = $("#replace").is(":checked");
    let notes = [];
    let reason = [];
    let values = [];
    let timestamp = [];
    const building_number = meterData[0].building_number;
    const commodity_tag = meterData[0].commodity_tag;
    const meter = meterData[0].meter;
    if (replaceData.length === 0) {
      alert("Please select at least one date");
      $("#overlay").hide();
    } else {
      if (checked === true) {
        replaceData.forEach(function(item) {
          if ($("#notes").val() === "") {
            notes.push(null);
          } else {
            notes.push($("#notes").val());
          }
          reason.push($("#reason").val());
          values.push(parseFloat(item.Expected));
          timestamp.push(item.Date);
        });
      } else {
        replaceData.forEach(function(item) {
          if ($("#notes").val() === "") {
            notes.push(null);
          } else {
            notes.push($("#notes").val());
          }
          reason.push($("#reason").val());
          values.push(null);
          timestamp.push(item.Date);
        });
      }

      $(".overlayMessage").text("Submitting Data. Please wait...");
      //This is the ajax call that posts replacement data to s3 using the replace data table
      $.ajax({
        //This route can be found under the routes folder under meterValidation in apiGateway.js
        url: "/postReplacement",
        method: "POST",
        //sends back data to the axios request in the controller folder
        data: {
          building_number: building_number,
          commodity_tag: commodity_tag,
          meter: meter,
          timestamp: JSON.stringify(timestamp),
          values: JSON.stringify(values),
          reason: JSON.stringify(reason),
          notes: JSON.stringify(notes),
        },
        error: function(jqXhr, textStatus, errorThrown) {
          if (jqXhr.status === 400) {
            alert("Invalid Request. Please try again.");
            $("#overlay").fadeOut();
          }
          if (jqXhr.status === 504) {
            $("#overlay").fadeOut();
            $(".edit").prop("checked", false);
            alert(
              "Server Error. Your data has been saved. Please hit the submit button again."
            );
          }
        },
      }).then(() => {
        replaceData.forEach((item, i) => {
          const row = $(`.tableData tbody tr:eq(${item.index})`);
          const R = (c) => row.children(`td:eq(${c})`);
          R(6).text(values[i] === null ? "-" : values[i]);
          R(7).text(reason[i]);
          R(8).text(notes[i] === null ? "-" : notes[i]);
        });
        $("#overlay").fadeOut();
        $(".overlayMessage").text("Getting data, this will take a few seconds");
        notes = [];
        timestamp = [];
        values = [];
        reason = [];
        replaceData = [];
        $(".edit").prop("checked", false);
        $("#replace").prop("checked", false);
        $("#notes").val("");
        $("#reason").val("Choose...");
        $(".successAlert")
          .html(`<div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> Your data has been successfully uploaded!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`);

        setTimeout(() => {
          $(".successAlert").empty();
        }, 3000);
      });
    }
  });

  $(".logout").click(function() {
    submitModel();
  });

  $(".saveAttributes").on("click", function() {
    $(
      this
    ).html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    <span class="visually-hidden">Loading...</span>`);
    submitModel();
  });

  //function to submit model attributes to postgres. The table that holds this information in postgres is called meter_tag_olsr_model
  const submitModel = () => {
    const str = $(".autoIgnored").text();
    let newStr = str.substring(0, str.length - 1);
    const building_number = meterData[0].building_number;
    const meter = $(".currentMeter").text();
    const commodity_tag = $(".currentCommodity").text();
    const x = $(".meterVariable").text();
    const base_temperature = $(".baseTemp").text();
    const auto_ignored_percentage = Number(newStr);
    const slope = Number($(".slope").text());
    const intercept = Number($(".intercept").text());
    const r2 = Number($(".r2").text());
    const std = Number($(".stdDev").text());
    const train_start = $(".start").text();
    const train_end = $(".end").text();

    $.ajax({
      type: "POST",
      //this route can be found under routes folder under meterValidation in apiGateway.js
      url: "/postModel",
      //sends back data to the axios request in the controller folder
      data: {
        building_number: building_number,
        meter: meter,
        commodity_tag: commodity_tag,
        train_start: train_start,
        train_end: train_end,
        x: x,
        auto_ignored_percentage: auto_ignored_percentage,
        base_temperature: base_temperature,
        r2: r2,
        slope: slope,
        intercept: intercept,
        std: std,
      },
    }).then((getModel) => {
      console.log(getModel);
      $(".saveAttributes").html(
        `<i style="margin: 0 auto" class="far fa-check-circle fa-2x text-center"></i>`
      );
      setTimeout(() => {
        $(".saveAttributes").html("Saved");
        $(".saveAttributes").attr("disabled", true);
      }, 4000);
    });
  };
});
