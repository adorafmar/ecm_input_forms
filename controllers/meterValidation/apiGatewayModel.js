require("dotenv").config();
const { Model_api_authorization } = require("../../models");
const axios = require("axios").default;
module.exports = {
  getModel: async (req, res) => {
    const {
      buildingNumber,
      meter,
    } = req.query;
    const email = req.user.email;

    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const config = {
        headers: {
          authorizationToken: token.token,
        },
      };
      const query =
        process.env.GET_MODEL_URL +
        `building_number=${buildingNumber}&meter=${meter}`;
      const response = await axios.get(query, config);
      return res.json(response.data);
    } catch (error) {
      console.error(error.message);
      return res.json(error.message);
    }
  },

  postModel: async (req, res) => {
    const email = req.user.email;
    let {
      building_number,
      meter,
      commodity_tag,
      train_start,
      train_end,
      x,
      auto_ignored_percentage,
      base_temperature,
      r2,
      slope,
      intercept,
      std,
    } = req.body;
    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const headers = {
        "Content-Type": "application/json",
        authorizationToken: token.token,
      };

      const data = JSON.stringify({
        building_number: building_number,
        meter: meter,
        commodity_tag: commodity_tag,
        train_start: train_start,
        train_end: train_end,
        x: x.toLowerCase(),
        auto_ignored_percentage: Number(auto_ignored_percentage),
        base_temperature:
          Number(base_temperature) === "--" ? null : Number(base_temperature),
        r2: Number(r2),
        slope: Number(slope),
        intercept: Number(intercept),
        std: Number(std),
        analyst: email,
      });
      const url = process.env.POST_MODEL_URL;
      const response = await axios.post(url, data, {
        headers: headers,
      });

      return res.json(response.data);
    } catch (error) {
      console.error(error);
      return res.json(error);
    }
  },

  postOngoingIssue: async (req, res) => {
    const { building_number, meter, notes } = req.body;
    const email = req.user.email;
    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const headers = {
        "Content-Type": "application/json",
        authorizationToken: token.token,
      };

      const data = {
        
          building_number: building_number,
          meter: meter,
          notes: notes,
          analyst: email,
        
      };

      const url = process.env.POST_ONGOING_ISSUE_URL;
      const response = await axios.post(url, data, {
        headers: headers,
      });

      return res.json(response.data);
    } catch (error) {
      console.error(error);
      return res.json(error);
    }
  },

  postReplacement: async (req, res) => {
    const {
      building_number,
      commodity_tag,
      meter,
      timestamp,
      values,
      reason,
      notes,
    } = req.body;
    const email = req.user.email;

    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const headers = {
        "Content-Type": "application/json",
        authorizationToken: token.token,
      };

      const data = JSON.stringify({
        analyst: email,
        building_number: building_number,
        commodity_tag: commodity_tag,
        meter: meter,
        data: {
          timestamp: JSON.parse(timestamp),
          value: JSON.parse(values),
          reason: JSON.parse(reason),
          notes: JSON.parse(notes),
        },
      });

      const url = process.env.POST_REPLACEMENT_URL;
      const response = await axios.post(url, data, {
        headers: headers,
      });

      return res.json(response.data);
    } catch (error) {
      console.error(error.message);
      return res.json(error.message);
    }
  },

  getReview: async (req, res) => {
    const {
      startTimestamp,
      endTimestamp,
    } = req.query;
    const email = req.user.email;

    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const config = {
        headers: {
          authorizationToken: token.token,
        },
      };
      const query =
        process.env.GET_REVIEW_URL +
        `start_timestamp=${startTimestamp}&end_timestamp=${endTimestamp}`;
      const response = await axios.get(query, config);
      return res.json(response.data);
    } catch (error) {
      console.error(error);
      return res.json(error.message);
    }
  },

  postReview: async (req, res) => {
    const email = req.user.email;
    let {
      building_number,
      meter,
      commodity_tag,
      train_start,
      train_end,
      analysis_start,
      analysis_end,
      x,
      auto_ignored_percentage,
      base_temperature,
      r2,
      slope,
      intercept,
      std,
    } = req.body;
    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const headers = {
        "Content-Type": "application/json",
        authorizationToken: token.token,
      };

      const data = JSON.stringify({
        building_number: building_number,
        meter: meter,
        commodity_tag: commodity_tag,
        train_start: train_start,
        train_end: train_end,
        analysis_start: analysis_start,
        analysis_end: analysis_end,
        x: x.toLowerCase(),
        auto_ignored_percentage: Number(auto_ignored_percentage),
        base_temperature:
          Number(base_temperature) === "--" ? null : Number(base_temperature),
        r2: Number(r2),
        slope: Number(slope),
        intercept: Number(intercept),
        std: Number(std),
        analyst: email,
      });
      const url = process.env.POST_REVIEW_URL;
      const response = await axios.post(url, data, {
        headers: headers,
      });

      return res.json(response.data);
    } catch (error) {
      console.error(error);
      return res.json(error);
    }
  },

  getTimeSeries: async (req, res) => {
    try {
      const {
        buildingNumber,
        commodity,
        meter,
        endTimestamp,
        startTimestamp,
      } = req.query;
      const email = req.user.email;

      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const config = {
        headers: {
          authorizationToken: token.token,
        },
      };

      const query =
        process.env.GET_TIME_SERIES_URL +
        `building_number=${buildingNumber}&commodity_tag=${commodity}&meter=${meter}&start_timestamp=${startTimestamp}&end_timestamp=${endTimestamp}`;
      const response = await axios.get(query, config);
      return res.json(response.data);
    } catch (error) {
      console.error(error.message);
      return res.json(error.message);
    }
  },

  getToClean: async (req, res) => {
    const { endTimestamp, startTimestamp, steward } = req.query;
    const email = req.user.email;
    try {
      if (!steward || steward === "All") {
        const token = await Model_api_authorization.findOne({
          where: {
            email: email,
          },
          attributes: ["token"],
        });

        const config = {
          headers: {
            authorizationToken: token.token,
          },
        };

        const query =
          process.env.GET_TO_CLEAN_URL +
          `end_timestamp=${endTimestamp}&start_timestamp=${startTimestamp}`;
        const response = await axios.get(query, config);

        return res.json(response.data);
      } else {
        const email = req.user.email;

        const token = await Model_api_authorization.findOne({
          where: {
            email: email,
          },
          attributes: ["token"],
        });

        const config = {
          headers: {
            authorizationToken: token.token,
          },
        };

        const query =
          process.env.GET_TO_CLEAN_URL +
          `end_timestamp=${endTimestamp}&start_timestamp=${startTimestamp}&steward_email=${steward}`;
        const response = await axios.get(query, config);

        return res.json(response.data);
      }
    } catch (error) {
      console.error(error);
      return res.json(error.message);
    }
  },

  getValidation: async (req, res) => {
    const {
      buildingNumber,
      commodity,
      meter,
      trainStart,
      trainEnd,
      analysisStart,
      analysisEnd,
    } = req.query;
    const email = req.user.email;

    try {
      const token = await Model_api_authorization.findOne({
        where: {
          email: email,
        },
        attributes: ["token"],
      });

      const config = {
        headers: {
          authorizationToken: token.token,
        },
      };
      const query =
        process.env.GET_VALIDATION_URL +
        `building_number=${buildingNumber}&commodity_tag=${commodity}&meter=${meter}&train_start=${trainStart}&train_end=${trainEnd}&analysis_start=${analysisStart}&analysis_end=${analysisEnd}`;
      const response = await axios.get(query, config);
      return res.json(response.data);
    } catch (error) {
      console.error(error);
      return res.json(error.message);
    }
  },
};
