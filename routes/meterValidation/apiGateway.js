const router = require('express').Router()
const gatewayController = require('../../controllers/meterValidation/apiGatewayModel')

router.get('/getModel', gatewayController.getModel);

router.post('/postModel', gatewayController.postModel);

router.post('/postOngoingIssue', gatewayController.postOngoingIssue)

router.post('/postReplacement', gatewayController.postReplacement)

router.get('/getReview', gatewayController.getReview)

router.post('/postReview', gatewayController.postReview)

router.get('/getTimeSeries', gatewayController.getTimeSeries)

router.get('/getToClean', gatewayController.getToClean)

router.get('/getValidation', gatewayController.getValidation)

module.exports = router